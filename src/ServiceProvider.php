<?php
namespace KDA\Layouts\FloatingUI;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
use KDA\Laravel\Traits\HasViews;
use KDA\Laravel\Traits\HasManifest;
use KDA\Laravel\Layouts\Facades\LayoutManager;

use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasViews;
    use HasManifest;
    protected $packageName ='layouts-floating-ui';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    public function register()
    {
        parent::register();
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){

        LayoutManager::registerRenderHook(
            'alpine.plugins',
            function(){
               return  Blade::render('<script defer src="https://cdn.jsdelivr.net/npm/@awcodes/alpine-floating-ui@3.x.x/dist/cdn.min.js"></script>');
            }
        );
    }
}
