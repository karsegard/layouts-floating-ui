let mix = require("laravel-mix");

mix
.setPublicPath('./')
.js("resources/js/floating-ui.js", "assets/js")
.version();